const yearStampThreshold = 2020;
const storedIdDigit = 2;
const transactionIdDigit = 3;

// use 9 digit code (0-9,A-Z), so the storage for each digit is 36
// 1 digit for year.
// 1 digit for month.
// 1 digit for day.
// 2 digits for storeId, 2 digits can cover 36 * 36 = 1296 stores, overkill.
// 3 digits for transactions, 36*36*36 = 46656 transactions.
// 1 digits for a hashCode, to do a basic verification.

function generateShortCode(storeId, transactionId) {
  const date = new Date();
  const yearStam = date.getFullYear() - yearStampThreshold;
  const month = date.getMonth();
  const day = date.getDate();
  const storeIdNumArray = parstIdToNumArray(storeId, storedIdDigit);
  const transactionIdNumArray = parstIdToNumArray(
    transactionId,
    transactionIdDigit
  );
  const numArray = [yearStam, month, day].concat(
    storeIdNumArray,
    transactionIdNumArray
  );
  const hashCode = getHashCode(numArray);
  numArray.push(hashCode);
  const shortCode = parstNumArrayToCode(numArray);

  return shortCode;
}

// TODO: Modify this function
function decodeShortCode(shortCode) {
  const numArray = parseCodeToNumArray(shortCode);
  if (!isValidNumArray(numArray)) return "Invalid";
  const yearStamp = numArray[0];
  const year = yearStamp + yearStampThreshold;
  const month = numArray[1];
  const day = numArray[2];
  const storeIdArray = numArray.slice(3, 3 + storedIdDigit);
  const transactionIdArray = numArray.slice(5, 5 + transactionIdDigit);
  const storeId = parseNumArrayToId(storeIdArray);
  const transactionId = parseNumArrayToId(transactionIdArray);

  return {
    storeId: storeId, // store id goes here,
    shopDate: new Date(year, month, day), // the date the customer shopped,
    transactionId: transactionId, // transaction id goes here
  };
}

function parstIdToNumArray(id, digit) {
  return parstIdtoNumArrayRcsvTask(id, digit).reverse();
}
function parstIdtoNumArrayRcsvTask(number, count) {
  if (count === 1) return [number];
  else {
    const dividedNum = Number.parseInt(number / 36);
    return [number % 36].concat(parstIdtoNumArrayRcsvTask(dividedNum, --count));
  }
}
function parseNumArrayToId(numArray) {
  return numArray.reduce((partialSum, num) => partialSum * 36 + num, 0);
}

function parstNumArrayToCode(numArray) {
  return numArray.reduce(
    (partialCode, num) => partialCode + parstNumToChar(num),
    ""
  );
}
function parstNumToChar(num) {
  if (num >= 0 && num <= 9) return num.toString();
  else if (num >= 10 && num <= 35) {
    const charCode = num - 10 + "A".charCodeAt(0);
    return String.fromCharCode(charCode);
  } else return "!";
}

function parseCodeToNumArray(code) {
  const numArray = [];
  for (let i = 0; i < code.length; i++) {
    const char = code[i];
    numArray.push(parseCharToNum(char));
  }
  return numArray;
}
function parseCharToNum(char) {
  if (char >= "0" && char <= "9") {
    return Number.parseInt(char);
  } else if (char >= "A" && char <= "Z") {
    const parsedNum = char.charCodeAt(0);
    const num = parsedNum - "A".charCodeAt(0) + 10;
    return num;
  } else return -1;
}

function getHashCode(numArray) {
  const sum = numArray.reduce((partialSum, num) => partialSum + num, 0);
  return sum % 36;
}

function isValidNumArray(numArray) {
  numArray.forEach((num) => {
    if (num === -1) return false;
  });
  const recordedHashCode = numArray[8];
  const numArrayBody = numArray.slice(0, 8);
  const desiredHashCode = getHashCode(numArrayBody);
  return recordedHashCode === desiredHashCode;
}

// ------------------------------------------------------------------------------//
// --------------- Don't touch this area, all tests have to pass --------------- //
// ------------------------------------------------------------------------------//
function RunTests() {
  var storeIds = [175, 42, 0, 9];
  var transactionIds = [9675, 23, 123, 7];

  storeIds.forEach(function (storeId) {
    transactionIds.forEach(function (transactionId) {
      var shortCode = generateShortCode(storeId, transactionId);
      var decodeResult = decodeShortCode(shortCode);
      $("#test-results").append(
        "<div>" + storeId + " - " + transactionId + ": " + shortCode + "</div>"
      );
      AddTestResult("Length <= 9", shortCode.length <= 9);
      AddTestResult("Is String", typeof shortCode === "string");
      AddTestResult("Is Today", IsToday(decodeResult.shopDate));
      AddTestResult("StoreId", storeId === decodeResult.storeId);
      AddTestResult("TransId", transactionId === decodeResult.transactionId);
    });
  });
}

function IsToday(inputDate) {
  // Get today's date
  var todaysDate = new Date();
  // call setHours to take the time out of the comparison
  return inputDate.setHours(0, 0, 0, 0) == todaysDate.setHours(0, 0, 0, 0);
}

function AddTestResult(testName, testResult) {
  var div = $("#test-results").append(
    "<div class='" +
      (testResult ? "pass" : "fail") +
      "'><span class='tname'>- " +
      testName +
      "</span><span class='tresult'>" +
      testResult +
      "</span></div>"
  );
}
